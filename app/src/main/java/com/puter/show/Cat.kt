package com.puter.show

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

data class Cat(
    var name: ObservableField<String> = ObservableField("Simon"),
    var age: ObservableInt = ObservableInt(7),
    var gender:ObservableField<String>? = null,
    var avatarUrl:ObservableField<String>? = null
)