package com.puter.show

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import com.puter.show.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val cat = Cat()
    private val catObs: CatObservable by lazy {
        CatObservable().apply {
            this.age = 22
            this.name = "Simon"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.cat = cat
        binding.catObs = catObs

        btn.setOnClickListener {
            cat.age.set((Math.random() * 199).toInt())
            cat.name.set("Pid Rilla")
            cat.gender?.set("male") ?: run {
                cat.gender = ObservableField("female")
                binding.invalidateAll()
            }

            cat.avatarUrl?.set("https://i.ytimg.com/vi/kVcifKSAJA4/mqdefault.jpg") ?: run {
                cat.avatarUrl = ObservableField("https://i.ytimg.com/vi/kVcifKSAJA4/mqdefault.jpg")
                binding.invalidateAll()
            }

            catObs.name = "Simon Pidoro"
            catObs.age = (Math.random() * 1000).toInt()
        }
    }

}