package com.puter.show

class Calculator {

    companion object {
        @JvmStatic
        fun sum(var1: Int, var2: Int): Int = var1 + var2

        @JvmStatic
        fun div(var1: Int, var2: Int): Int = var1 / var2

        @JvmStatic
        fun mul(var1: Int, var2: Int): Int = var1 * var2

        @JvmStatic
        fun square(var1: Int): Int = var1 * var1
    }
}