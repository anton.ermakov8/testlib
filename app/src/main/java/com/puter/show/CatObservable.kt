package com.puter.show

import android.widget.SeekBar
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR


class CatObservable : BaseObservable() {

    @Bindable
    var name: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }

    @Bindable
    var age: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.age)
        }

    @Bindable
    var seek: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.seek)
        }

}
