package com.nixesea.hellolib

import android.os.Handler
import android.util.Log

class HelloLib {

    companion object {
        var counter = 0L
        val handler: Handler by lazy { Handler() }

        @JvmStatic
        fun init(duration: Long, msg: String) {
            handler.postDelayed(object : Runnable {
                override fun run() {
                    Log.i("HelloLib", "$msg #$counter")
                    counter++
                    handler.postDelayed(this, duration)
                }
            }, duration)
        }

    }
}